﻿using AutoMapper;
using Luizalabs.WishListAPI.Model;

namespace Luizalabs.WishListAPI.Web.API.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, Model.UserDto>();
            CreateMap<Model.UserDto, User>();

            CreateMap<Model.WishItem, Product>();
            CreateMap<Product, Model.WishItem>();

            CreateMap<Product, Model.ProductDto>()
                .ForMember(
                    dest => dest.idProduct,
                    opt => opt.MapFrom(src => src.id));
            CreateMap<Model.ProductDto, Product>()
                .ForMember(
                    dest => dest.id,
                    opt => opt.MapFrom(src => src.idProduct));


        }
    
    }
}
