﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Luizalabs.WishListAPI.Model;
using Luizalabs.WishListAPI.Web.API.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Luizalabs.WishListAPI.Web.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class WishesController : ControllerBase
    {
        private readonly Business.Interface.IWishList _bus;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public WishesController(Business.Interface.IWishList bus, IMapper mapper, ILogger<WishesController> logger)
        {
            _logger = logger;
            _mapper = mapper;
            _bus = bus;
        }

        // GET products/{userId}/?page_size=10&page=1
        [HttpGet("{userId}")]
        public ActionResult<IEnumerable<WishItem>> Get(string userId, int page_size = 10, int page = 1)
        {
            _logger.LogInformation($"url: wishes/{HttpContext.Request.QueryString.Value}");

            try
            {
                var users = _bus.GetWishList(userId, page_size, page);
                var result = _mapper.Map<List<WishItem>>(users);
                return result;
            }
            catch (AutoMapperMappingException ex)
            {
                _logger.LogError($"Automapper Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Erro ao mapear entidade",
                    code = 500
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Ocorreu um erro interno.",
                    code = 500
                });
            }
        }

        // POST wishes/{userId}
        [HttpPost("{userId}")]
        public IActionResult Post(string userId, [FromBody] List<ProductDto> wishes)
        {
            _logger.LogInformation($"Request: {{userId: {userId} }}");

            try
            {
                UserDto dto = new UserDto();
                dto.id = userId;
                dto.products = wishes;

                var user = _mapper.Map<User>(dto);

                User message = _bus.Create(user);
                return StatusCode(201);
            }
            catch (AutoMapperMappingException ex)
            {
                _logger.LogError($"Automapper Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Erro ao mapear entidade",
                    code = 500
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Ocorreu um erro interno.",
                    code = 500
                });
            }
        }

        // POST wishes/{userId}
        [HttpDelete("{userId}/{productId}")]
        public IActionResult Delete(string userId, string productId)
        {
            _logger.LogInformation($"Request: {{userId: {userId}, productId: {productId}}}");

            try
            {
                _bus.Delete(userId, productId);
                return StatusCode(201);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Ocorreu um erro interno.",
                    code = 500
                });
            }
        }
    }
}