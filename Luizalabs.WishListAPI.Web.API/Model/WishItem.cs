﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Luizalabs.WishListAPI.Web.API.Model
{
    public class WishItem
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
