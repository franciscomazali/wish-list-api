﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Luizalabs.WishListAPI.Web.API.Model
{
    public class UserDto
    {
        public string id { get; set; }
        public List<ProductDto> products { get; set; }
    }
}
