﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luizalabs.WishListAPI.Model
{
    public class User
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public List<Product> products { get; set; }
    }
}
