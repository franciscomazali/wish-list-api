﻿using Luizalabs.WishListAPI.Business.Interface;
using Luizalabs.WishListAPI.Firebase;
using Luizalabs.WishListAPI.Model;
using System.Linq;
using System.Collections.Generic;
using static Luizalabs.WishListAPI.Utils.LinqExtentions;

namespace Luizalabs.WishListAPI.Business
{
    public class WishList : IWishList
    {
        public User Create(User model)
        {
            FireWishList fire = new FireWishList();
            User message = fire.Push(model);

            return message;
        }

        public void Delete(string userId, string productId)
        {
            FireWishList fire = new FireWishList();
            fire.RemoveProduct(userId, productId);
        }

        public List<Product> GetWishList(string userId, int page_size, int page)
        {
            FireWishList fire = new FireWishList();
            var users = fire.GetProducts(userId);

            if (users != null)
            {
                users = users.Page(page, page_size).ToList();

                return users.ToList();
            }

            return new List<Product>();
        }
    }
}
