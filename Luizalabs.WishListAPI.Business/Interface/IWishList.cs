﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luizalabs.WishListAPI.Business.Interface
{
    public interface IWishList
    {
        List<Model.Product> GetWishList(string userId, int page_size, int page);
        Model.User Create(Model.User model);
        void Delete(string userId, string productId);
    }
}
