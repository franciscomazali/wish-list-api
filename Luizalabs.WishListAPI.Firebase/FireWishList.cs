﻿using FirebaseNetAdmin;
using FirebaseNetAdmin.Configurations.ServiceAccounts;
using FirebaseNetAdmin.Firebase.Commands;
using Luizalabs.WishListAPI.Model;
using System.Collections.Generic;

namespace Luizalabs.WishListAPI.Firebase
{
    public class FireWishList
    {
        private readonly FirebaseAdmin client;

        public FireWishList()
        {
            var credentials = new JSONServiceAccountCredentials("key/wishlistapi-luiza-firebase-adminsdk-2tnz1-73493d753c.json");
            client = new FirebaseAdmin(credentials);
        }

        public User Push(User user)
        {
            var refUser = client.Database.Ref($"users/{user.id}");
            var userDb = refUser.Get<User>();

            if (userDb.products == null)
            {
                userDb.products = new List<Product>();
            }

            user.products.ForEach(p => {
                var refProd = client.Database.Ref($"products/{p.id}");
                var prod = refProd.Get<Product>();
                p.name = prod.name;     
            });


            userDb.products.AddRange(user.products);

            var result = refUser.Set(userDb);

            return result;
        }

        public List<Product> GetProducts(string id)
        {
            var db = client.Database.Ref($"users/{id}/products");
            var products = db.GetArray<Product>();

            return products;

        }

        public User RemoveProduct(string userId, string productId)
        {
            var db = client.Database.Ref($"users/{userId}");
            var user = db.Get<User>();

            user.products.RemoveAll(p => p.id == productId);

            var result = db.Set(user);
            return result;
        }
    }
}
